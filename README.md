# Awesomes links-misc

My list of awesomes links that interested me. Saved for future.

# Test automation
### BDD
- https://automationpanda.com/2017/01/30/bdd-101-writing-good-gherkin/ - good practices about Gherkin

# Databases
- https://dbeaver.io/ - database tool for developers

# UI/UX
### Links about ui/ux:
- http://uigoodies.com - lists of ui libraries
